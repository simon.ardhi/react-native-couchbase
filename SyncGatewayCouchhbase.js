{
    "log": ["*"],
    "databases": {
      "notes": {
        "server": "prudential:",
        "users": { "GUEST": { "disabled": false, "admin_channels": ["*"] } },
        "allow_empty_password": true,
        "sync": `
          function(doc, oldDoc){
            channel('!');
          }
        `
      }
    },
    "facebook": {
      "register": true
    }
  }